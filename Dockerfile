# inspired by https://github.com/python-poetry/poetry/issues/1178#issuecomment-517812277

FROM python:3.9-slim AS base

LABEL maintainer "tech-data@siliconvalleydsa.org"

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.1.4 \
  VIRTUAL_ENV=/opt/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Place apt dependencies that the app needs here
RUN apt-get update \
  && apt-get install --no-install-recommends -y \
  default-libmysqlclient-dev \
  netcat \
  && rm -rf /var/lib/apt/lists/*

FROM base AS poetry

# Place apt dependencies that the build process needs here
RUN apt-get update \
  && apt-get install --no-install-recommends -y \
  curl \
  build-essential \
  python3-dev \
  && pip install --no-cache-dir -U pip \
  && pip install --no-cache-dir "poetry==$POETRY_VERSION" \
  && poetry --version \
  # configure poetry
  && python -m venv $VIRTUAL_ENV \
  && poetry config virtualenvs.create false \
  # cleanup
  && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY poetry.lock pyproject.toml ./
RUN . /opt/venv/bin/activate && poetry install --no-interaction --no-ansi -vvv

FROM base AS runtime

WORKDIR /usr/src/app

COPY --from=poetry /opt/venv /opt/venv
COPY pan pan

ENTRYPOINT ["python", "pan/app.py"]
