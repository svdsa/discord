import logging
import os
import signal
import string
import sys
from asyncio.events import get_event_loop
from datetime import datetime
from typing import cast

import discord
import dotenv
import nanoid
import redis
from fastapi import FastAPI, Form
from fastapi.params import Form as FormCls
from hypercorn.asyncio import serve
from hypercorn.config import Config
from pydantic import BaseModel

# https://stackoverflow.com/questions/27480967/why-does-the-asyncios-event-loop-suppress-the-keyboardinterrupt-on-windows
signal.signal(signal.SIGINT, signal.SIG_DFL)

dotenv.load_dotenv()
GUILD_ID = os.getenv("DISCORD_GUILD_ID")
if GUILD_ID is None:
    logging.error(
        "Must provide DISCORD_GUILD_ID. Right-click on server name > Copy ID."
    )
    sys.exit(1)

MEMBER_ROLE_ID = os.getenv("DISCORD_MEMBER_ROLE_ID")
if MEMBER_ROLE_ID is None:
    logging.error(
        "Must provide DISCORD_MEMBER_ROLE_ID. Server settings > Roles > right-click on the role you want to add upon confirmation > Copy ID."
    )
    sys.exit(2)

BOT_TOKEN = os.getenv("DISCORD_BOT_TOKEN")
if BOT_TOKEN is None:
    logging.error(
        "Must provide DISCORD_BOT_TOKEN. Visit https://discord.com/developers/applications > your app > Bot > Token > Copy."
    )
    sys.exit(3)

REDIS_HOST = os.getenv("REDIS_HOST")
if REDIS_HOST is None:
    logging.error("Must provide REDIS_HOST.")
    sys.exit(4)

REDIS_PORT = os.getenv("REDIS_PORT")
if REDIS_PORT is None:
    logging.error("Must provide REDIS_PORT.")
    sys.exit(5)


store = redis.Redis(host=REDIS_HOST, port=int(REDIS_PORT))


def render_welcome_intro(member: discord.Member):
    return f"""Hey {member.display_name}! Welcome to the SV DSA Social!

I'm a bot and I'm here to help verify your membership.

It's easy to verify your membership! Here's how:"""


def render_welcome_step1():
    return f"""1️⃣ Make sure you're a member of DSA. If you aren't, you can join here: https://www.dsausa.org/join. You can also request a dues waiver at https://act.dsausa.org/survey/dueswaiver/."""


def render_welcome_step2():
    return f"""2️⃣ (If you already have a login for the DSA national forum, you can skip this step.) Go to https://optin.dsausa.org and enter the email you used for your DSA membership to request an account."""


def render_welcome_step3():
    return f"""3️⃣ Go to https://proof.dsausa.org, log in, and paste this email into the box:"""


def render_welcome_email_step(email: str):
    return f"""**{email}**"""


def render_welcome_footer():
    return f"""Then, press "Send proof of membership". It'll send an email to me.

Once I receive the proof email from DSA, I'll verify you and you'll get access to the rest of the channels!

If you have any questions, reach out on <#816162476836126780> and the fine folks there will help you out.

💖
"""


def generate_random_email() -> str:
    return f'discord-{nanoid.generate(string.ascii_lowercase + string.digits)}@verify.siliconvalleydsa.org'


def save_email_to_member_mapping(email: str, member: discord.Member):
    store.set(email, member.id)
    store.set(member.id, email)


async def handle_member_join(member: discord.Member):
    if (existing_email := store.get(member.id)) is not None:
        logging.info(
            f"member with id {id} already exists, sending existing email {existing_email}"
        )
        await member.send("Good to see you again! Check above for steps. Email again:")
        await member.send(
            render_welcome_email_step(cast(bytes, existing_email).decode('utf-8'))
        )
    else:
        email = generate_random_email()
        save_email_to_member_mapping(email, member)
        logging.info(f"member with id {id} is new, using new email {email}")
        await member.send(
            render_welcome_intro(
                member,
            )
        )
        await member.send(render_welcome_step1())
        await member.send(render_welcome_step2())
        await member.send(render_welcome_step3())
        await member.send(render_welcome_email_step(email))
        await member.send(render_welcome_footer())


async def send_confirmation_message(member: discord.Member):
    await member.send(
        """📫 Just got your email and confirmed. You should have access now!

If you still don't have access, let us know at <#816162476836126780>.

😸"""
    )


async def clear_mapping(email: str, member: discord.Member):
    store.delete(email)
    store.delete(member.id)


class MyClient(discord.Client):
    async def on_ready(self):
        logging.debug('Logged in as')
        logging.debug(self.user.name)
        logging.debug(self.user.id)
        logging.debug('------')

    async def on_member_join(self, member: discord.Member):
        await handle_member_join(member)


intents = discord.Intents.default()
intents.members = True

client = MyClient(intents=intents)

app = FastAPI()


@app.get("/health")
async def health():
    print("Healthcheck")
    print(f"Health: {store.time()}")
    return client.latency


@app.post("/mailgun")
async def verify_email(
    sender: str = Form(...),
    recipient: str = Form(...),
    subject: str = Form(...),
    body: str = Form(..., alias="body-plain"),
):
    if (member_id := store.get(str(recipient))) is None:
        return
    if not str(sender).endswith("dsausa.org"):
        raise ValueError(f"Invalid sending domain. Got {sender} instead")
    if 'Proof of Membership' not in str(subject):
        raise ValueError(f"Incorrect subject line. Got {subject} instead")

    guild = await client.fetch_guild(GUILD_ID)
    member = await guild.fetch_member(member_id.decode('utf-8'))
    try:
        role = guild.get_role(int(MEMBER_ROLE_ID))
        if role is None:
            raise ValueError("Couldn't find member role on guild")
        await member.add_roles(role)

        # update role
        await send_confirmation_message(member)
        await clear_mapping(recipient, member)
    except Exception as e:
        await member.send(
            f"😿 I couldn't give you access! Contact <#816162476836126780> and let them know I said '{e}'..."
        )
        logging.exception(e)


@app.get("/test/{member_id}")
async def test(member_id: str):
    guild = await client.fetch_guild(GUILD_ID)
    member = await guild.fetch_member(member_id)
    await handle_member_join(member)


if __name__ == "__main__":
    loop = get_event_loop()

    try:
        loop.create_task(client.start(BOT_TOKEN))
        config = Config()
        config.bind = ['0.0.0.0:8000']

        loop.run_until_complete(serve(app, config))
    except Exception as e:
        loop.stop()
